package collision;

import rendering.Renderable;

/**
 * Created by Vesko on 6.8.2018 г..
 */
public class Collision {
    public static final int TOP = 0;
    public static final int BOTOM = 1;
    public static final int LEFT = 2;
    public static final int RIGHT = 3;

    private int direction;

    private Renderable item;

    public Collision(int direction, Renderable item) {
        this.direction = direction;
        this.item = item;
    }

    public int getDirection() {
        return direction;
    }

    public Renderable getItem() {
        return item;
    }
}
