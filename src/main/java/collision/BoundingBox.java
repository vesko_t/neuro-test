package collision;

import org.joml.Vector3f;

/**
 * Created by vesko on 26.01.18.
 */
public class BoundingBox {
    private Vector3f position;
    private Vector3f scale;

    public BoundingBox(Vector3f position, Vector3f scale) {
        this.position = position;
        this.scale = scale;
    }

    public Vector3f leftBottomCorner() {
        return new Vector3f(position.x - scale.x(), position.y - scale.y, 0);
    }

    public Vector3f rightBottomCorner() {
        return new Vector3f(position.x + scale.x(), position.y + scale.y, 0);
    }

    public Vector3f leftTopCorner() {
        return new Vector3f(position.x - scale.x(), position.y + scale.y, 0);
    }

    public Vector3f rightTopCorner() {
        return new Vector3f(position.x + scale.x(), position.y + scale.y, 0);
    }

    public boolean isTop(BoundingBox box) {
        return box.getPosition().x > leftTopCorner().x && box.getPosition().x < rightTopCorner().x && box.getPosition().y > position.y;
    }

    public boolean isBottom(BoundingBox box) {
        return box.getPosition().x > leftBottomCorner().x && box.getPosition().x < rightBottomCorner().x && box.getPosition().y < position.y;
    }

    public boolean isLeft(BoundingBox box) {
        return box.getPosition().y > leftBottomCorner().y && box.getPosition().y < leftTopCorner().y && box.getPosition().x < position.x;
    }

    public boolean isRight(BoundingBox box) {
        return box.getPosition().y > rightBottomCorner().y && box.getPosition().y < rightTopCorner().y && box.getPosition().x > position.x;
    }

    public boolean isColliding(BoundingBox box) {
        return position.x - getWidth() / 2 < box.getPosition().x + box.getWidth() / 2 &&
                position.x + getWidth() / 2 > box.getPosition().x - box.getWidth() / 2 &&
                position.y - getHeight() / 2 < box.getPosition().y + box.getHeight() / 2 &&
                position.y + getHeight() / 2 > box.getPosition().y - box.getHeight() / 2;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public Vector3f getScale() {
        return scale;
    }

    public void setScale(Vector3f scale) {
        this.scale = scale;
    }

    public float getHeight() {
        return scale.y() * 2;
    }

    public float getWidth() {
        return scale.x() * 2;
    }

    public void addPosition(Vector3f vector3f) {
        position.add(vector3f);
    }
}
