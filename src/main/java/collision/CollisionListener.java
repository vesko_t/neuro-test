package collision;

import java.util.List;

/**
 * Created by Vesko on 6.8.2018 г..
 */
public interface CollisionListener {

    void onCollision(List<Collision> collisions);

}
