package rendering;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.BufferUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;

/**
 * Created by Veso on 22.1.2018 г..
 */
public class Shader {
    private int program;

    public Shader(String filename) {
        createShader(filename);
    }

    /**
     * creates shader looking for vertex.glsl and fragment.glsl in shaders/filename
     * @param filename folder name
     */
    private void createShader(String filename) {
        program = glCreateProgram();

        int vs = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vs, loadShader(filename + "/vertex"));
        glCompileShader(vs);
        if (glGetShaderi(vs, GL_COMPILE_STATUS) != 1) {
            System.err.println("Vertex error\n" + glGetShaderInfoLog(vs));
            System.exit(1);
        }

        int fs = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fs, loadShader(filename + "/fragment"));
        glCompileShader(fs);
        if (glGetShaderi(fs, GL_COMPILE_STATUS) != 1) {
            System.err.println("Fragment error\n" + glGetShaderInfoLog(fs));
            System.exit(1);
        }

        glAttachShader(program, vs);
        glAttachShader(program, fs);

        glBindAttribLocation(program, 0, "vertices");
        glBindAttribLocation(program, 1, "textures");

        glLinkProgram(program);
        if (glGetProgrami(program, GL_LINK_STATUS) != 1) {
            System.err.println("Link error\n" + glGetProgramInfoLog(program));
            System.exit(1);
        }

        glValidateProgram(program);
        if (glGetProgrami(program, GL_VALIDATE_STATUS) != 1) {
            System.err.println("Validate error \n" + glGetProgramInfoLog(program));
            System.exit(1);
        }
    }

    public void setUniform(String name, int value) {
        int location = glGetUniformLocation(program, name);
        if (location != -1)
            glUniform1i(location, value);
    }

    public void setUniform(String name, float value) {
        int location = glGetUniformLocation(program, name);
        if (location != -1)
            glUniform1f(location, value);
    }

    public void setUniform(String name, Matrix4f value) {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
        value.get(buffer);
        int location = glGetUniformLocation(program, name);
        if (location != -1) {
            glUniformMatrix4fv(location, false, buffer);
        }
    }

    public void setUniform(String name, Vector3f value) {
        int location = glGetUniformLocation(program, name);
        if (location != -1) {
            glUniform3f(location, value.x, value.y, value.z);
        }
    }

    public void setUniform(String name, Vector2f value) {
        int location = glGetUniformLocation(program, name);
        if (location != -1) {
            glUniform2f(location, value.x, value.y);
        }
    }

    public void setUniform(String name, Vector4f value) {
        int location = glGetUniformLocation(program, name);
        if (location != -1) {
            glUniform4f(location, value.x, value.y, value.z, value.w);
        }
    }

    public void bind() {
        glUseProgram(program);
    }

    private String loadShader(String fileName) {
        BufferedReader reader;
        StringBuilder output = new StringBuilder();

        InputStream is = Shader.class.getResourceAsStream("/shaders/" + fileName + ".glsl");
        try {
            reader = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }

            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return output.toString();
    }

}
