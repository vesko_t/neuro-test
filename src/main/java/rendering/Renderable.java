package rendering;

import collision.BoundingBox;
import graphics.MathModel;
import graphics.Texture;
import org.joml.Matrix4f;
import org.joml.Vector3f;

/**
 * Created by vesko on 05.02.18.
 */
public class Renderable {

    protected Texture texture;

    protected Matrix4f matrix = new Matrix4f().identity();

    protected Vector3f position;

    protected Shader shader;

    protected MathModel model = new MathModel();

    protected boolean inJump = false;

    protected Vector3f velocity = new Vector3f(0, 0, 0);

    protected float jumpCount = 1;

    protected boolean falling = false;

    protected int fallCount = 20;

    protected boolean canMoveRight = true;

    protected boolean canMoveLeft = true;

    protected boolean isMovingRight = false;

    protected boolean isMovingLeft = false;

    protected boolean canJump = true;

    protected boolean canFall = true;

    protected Vector3f scale;

    protected BoundingBox boundingBox;

    protected boolean shouldBeFalling;

    public Renderable(Texture texture, Shader shader, Vector3f scale, Vector3f position) {
        this.texture = texture;
        this.shader = shader;
        this.scale = scale;
        this.position = position;
        matrix.scale(scale);
    }

    public void render() {
        matrix.setTranslation(position);
        shader.bind();
        shader.setUniform("sampler", 0);
        shader.setUniform("projection", Camera.getProjection());
        shader.setUniform("model", matrix);
        texture.bind(0);
        model.render();
    }

    public Texture getTexture() {
        return texture;
    }

    public Matrix4f getMatrix() {
        return matrix;
    }

    public Vector3f getPosition() {
        return position;
    }

    public Shader getShader() {
        return shader;
    }

    public MathModel getModel() {
        return model;
    }

    public void setModel(MathModel model) {
        this.model = model;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public void setMatrix(Matrix4f matrix) {
        this.matrix = matrix;
    }

    public void setShader(Shader shader) {
        this.shader = shader;
    }

    public boolean isInJump() {
        return inJump;
    }

    public void setInJump(boolean inJump) {
        this.inJump = inJump;
    }

    public Vector3f getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector3f velocity) {
        this.velocity = velocity;
    }

    public float getJumpCount() {
        return jumpCount;
    }

    public void setJumpCount(float jumpCount) {
        this.jumpCount = jumpCount;
    }

    public boolean isFalling() {
        return falling;
    }

    public void setFalling(boolean falling) {
        this.falling = falling;
    }

    public int getFallCount() {
        return fallCount;
    }

    public void setFallCount(int fallCount) {
        this.fallCount = fallCount;
    }

    public boolean isCanMoveRight() {
        return canMoveRight;
    }

    public void setCanMoveRight(boolean canMoveRight) {
        this.canMoveRight = canMoveRight;
    }

    public boolean isCanMoveLeft() {
        return canMoveLeft;
    }

    public void setCanMoveLeft(boolean canMoveLeft) {
        this.canMoveLeft = canMoveLeft;
    }

    public boolean isMovingRight() {
        return isMovingRight;
    }

    public void setMovingRight(boolean movingRight) {
        isMovingRight = movingRight;
    }

    public boolean isMovingLeft() {
        return isMovingLeft;
    }

    public void setMovingLeft(boolean movingLeft) {
        isMovingLeft = movingLeft;
    }

    public boolean isCanJump() {
        return canJump;
    }

    public void setCanJump(boolean canJump) {
        this.canJump = canJump;
    }

    public boolean isCanFall() {
        return canFall;
    }

    public void setCanFall(boolean canFall) {
        this.canFall = canFall;
    }

    public Vector3f getScale() {
        return scale;
    }

    public void setScale(Vector3f scale) {
        this.scale = scale;
    }

    public BoundingBox getBoundingBox() {
        return boundingBox;
    }

    public void setBoundingBox(BoundingBox boundingBox) {
        this.boundingBox = boundingBox;
    }

    public boolean isShouldBeFalling() {
        return shouldBeFalling;
    }

    public void setShouldBeFalling(boolean shouldBeFalling) {
        this.shouldBeFalling = shouldBeFalling;
    }
}
