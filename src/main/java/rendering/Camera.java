package rendering;

import entities.Entity;
import io.Window;
import org.joml.Matrix4f;
import org.joml.Vector3f;

/**
 * Created by Veso on 22.1.2018 г..
 */
public class Camera {
    private static Vector3f position;
    private static Matrix4f projection;
    private static int width;
    private static int height;

    private Window window;

    public Camera(Window window) {
        width = 1280;
        height = 720;
        this.window = window;
        position = new Vector3f(width / -2, height / -2, 0);
        projection = new Matrix4f().ortho2D(-width / 2, width / 2, -height / 2, height / 2);
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public static Matrix4f getProjection() {
        return projection.translate(position, new Matrix4f());
    }

    public Matrix4f getUnmovedProjection() {
        return projection;
    }

    public Vector3f getMousePositionWorld() {
        Vector3f pos = new Vector3f();
        position.mul(-1, pos);

        return pos.add(window.getInput().getMousePositionCamera());
    }

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }

    /**
     * Checks if an Entity is in view of the camera
     *
     * @param renderable the entity to check
     * @return is in view
     */
    public static boolean isInView(Renderable renderable) {
        return !(renderable.getPosition().x < -position.x - (getWidth() / 2)) ||
                !(renderable.getPosition().x > -position.x + (getWidth() / 2)) ||
                !(renderable.getPosition().y < -position.y - getHeight() / 2) ||
                !(renderable.getPosition().y > -position.y + getHeight() / 2);
    }

    /**
     * Follow the player check if it's at the ends of the map
     *
     * @param player player
     */
    public void update(Entity player) {
        Vector3f negativePlayerPos = new Vector3f(-player.getPosition().x, -player.getPosition().y, 0);

        position = negativePlayerPos;
    }
}
