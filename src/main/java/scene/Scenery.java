package scene;

import collision.BoundingBox;
import graphics.Texture;
import org.joml.Vector3f;
import rendering.Renderable;
import rendering.Shader;

/**
 * Created by Vesko on 6.8.2018 г..
 */
public class Scenery extends Renderable {


    public Scenery(Texture texture, Shader shader, Vector3f scale, Vector3f position) {
        super(texture, shader, scale, new Vector3f(position.x + scale.x, position.y - scale.y, 0));
        boundingBox = new BoundingBox(this.position, scale);
    }
}
