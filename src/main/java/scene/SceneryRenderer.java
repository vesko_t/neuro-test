package scene;

import rendering.Camera;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vesko on 6.8.2018 г..
 */
public class SceneryRenderer {

    private List<Scenery> sceneries = new ArrayList<Scenery>();

    private static List<Scenery> inView = new ArrayList<Scenery>();

    public List<Scenery> getSceneries() {
        return sceneries;
    }

    public static List<Scenery> getInView() {
        return inView;
    }

    public void render() {
        inView = new ArrayList<Scenery>();
        for (Scenery scenery : sceneries) {
            if (Camera.isInView(scenery)) {
                scenery.render();
                inView.add(scenery);
            }
        }
    }

}
