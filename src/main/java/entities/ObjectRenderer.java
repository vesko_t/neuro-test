package entities;

import collision.Collision;
import org.joml.Vector3f;
import rendering.Camera;
import scene.Scenery;
import scene.SceneryRenderer;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.ARBImaging.GL_BLEND_COLOR;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.GL_BLEND_SRC_ALPHA;

/**
 * Created by vesko on 23.01.18.
 */
public class ObjectRenderer {

    private List<Entity> entities = new ArrayList<Entity>();

    private static List<Entity> inView = new ArrayList<Entity>();

    public void update(float delta) {
        inView = new ArrayList<Entity>();
        for (Entity entity : entities) {
            List<Collision> collisions = new ArrayList<Collision>();
            for (Scenery scenery : SceneryRenderer.getInView()) {
                if (scenery.getBoundingBox().isColliding(entity.getBoundingBox())) {
                    int direction = -1;

                    Vector3f force = new Vector3f(0,-entity.getVelocity().y,0);

                    entity.addPosition(force);


                    /*if (scenery.getBoundingBox().isTop(entity.getBoundingBox())) {
                        direction = Collision.TOP;
                    }
                    if (scenery.getBoundingBox().isBottom(entity.getBoundingBox())) {
                        direction = Collision.BOTOM;
                    }

                    if (scenery.getBoundingBox().isLeft(entity.getBoundingBox())) {
                        direction = Collision.LEFT;
                    }
                    if (scenery.getBoundingBox().isRight(entity.getBoundingBox())) {
                        direction = Collision.RIGHT;
                    }*/
                    Collision collision = new Collision(direction, scenery);
                    collisions.add(collision);
                }
            }
            entity.onCollision(collisions);
            entity.update(delta);
            if (Camera.isInView(entity)) {
                inView.add(entity);
            }
        }
    }

    public static List<Entity> getInView() {
        return inView;
    }

    public void renderLights() {

        glEnable(GL_BLEND_COLOR);
        glBlendFunc(GL_BLEND_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        for (Entity entity : entities) {
            if (entity.getLight() != null) {
                entity.renderLight();
            }
        }
    }

    public void render() {
        for (Entity entity : inView) {
            entity.render();
        }
    }

    public List<Entity> getEntities() {
        return entities;
    }
}
