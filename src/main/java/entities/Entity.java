package entities;

import collision.BoundingBox;
import collision.Collision;
import collision.CollisionListener;
import graphics.Texture;
import graphics.lighting.Lightable;
import io.Input;
import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;
import rendering.Shader;

import java.util.List;

/**
 * Created by vesko on 23.01.18.
 */
public class Entity extends Lightable implements CollisionListener {

    protected boolean canJump = true;

    protected boolean isAlive = true;

    protected Float health = 20f;

    public Entity(Vector3f position, Vector3f scale, Texture texture, boolean lightable) {
        super(texture, new Shader("player"), scale, position);
        boundingBox = new BoundingBox(position, scale);
        if (lightable) {
            //light = LightUtils.createLight(position);
        }
    }

    public void update(float delta) {
        updateJumping(delta, Input.isKeyDown(GLFW.GLFW_KEY_SPACE));
        updateFalling(delta);
        updateMovement(delta, Input.isKeyDown(GLFW.GLFW_KEY_A), Input.isKeyDown(GLFW.GLFW_KEY_D));
        position.add(velocity);
        boundingBox.setPosition(position);
        shouldBeFalling = true;
        canMoveRight = true;
        canMoveLeft = true;
    }

    private void updateFalling(float delta) {
        velocity.y += -((500 - fallCount * 10) + scale.y) * delta;
        //velocity.y = -1;
        if (fallCount > 0) {
            fallCount -= 1;
        }
    }

    private void updateJumping(float delta, boolean shouldJump) {
        if (shouldJump) {
            velocity.y += ((500 - jumpCount * 10) + scale.y) * delta;
            jumpCount += 1.5f;
        }
        if (jumpCount >= 35) {
            jumpCount = 1;
            inJump = false;
        }
    }

    private void updateMovement(float delta, boolean left, boolean right) {
        if (right && canMoveRight) {
            velocity.x = (375 - scale.x) * delta;
        } else if (left && canMoveLeft) {
            velocity.x = -(375 - scale.x) * delta;
        } else {
            velocity.x = 0;
        }
    }

    public void onCollision(List<Collision> collisions) {
        for (Collision collision : collisions) {
            System.out.println(collision.getDirection());
            if (collision.getDirection() == Collision.BOTOM)
                System.out.println("bottom");
            if (collision.getDirection() == Collision.TOP) {
                System.out.println("top");
                shouldBeFalling = false;
            }
            if (collision.getDirection() == Collision.LEFT) {
                System.out.println("left");
                canMoveRight = false;
            }
            if (collision.getDirection() == Collision.RIGHT) {
                System.out.println("right");
                canMoveLeft = false;
            }
        }
    }

    protected void addPosition(Vector3f vector3f) {
        position.add(vector3f);
        boundingBox.setPosition(position);
    }

    public Float getHealth() {
        return health;
    }

    public boolean isAlive() {
        return isAlive;
    }
}
