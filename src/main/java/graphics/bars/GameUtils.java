package graphics.bars;

import rendering.Shader;
import org.joml.Vector4f;

/**
 * Created by vesko on 15.02.18.
 */
public class GameUtils {

    private GameUtils() {
    }

    public static Bar createBar(Vector4f color, float positon) {
        return new Bar(new Shader("bars"), color, positon);
    }
}
