package graphics.bars;

import graphics.MathModelNoTexture;
import rendering.Camera;
import rendering.Shader;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;

/**
 * Created by vesko on 13.02.18.
 */
public class Bar {
    private Vector3f position = new Vector3f();
    private MathModelNoTexture model = new MathModelNoTexture();
    private Shader shader;
    private Vector4f color;

    private float height = 20;
    private float value = 100;

    private float y;

    Bar(Shader shader, Vector4f color, float y) {
        this.shader = shader;
        this.color = color;

        this.y = y;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public float getHeight() {
        return height;
    }

    public float getValue() {
        return value;
    }

    public void render(Camera camera) {
        Matrix4f matrix = new Matrix4f().identity().scale(new Vector3f(value * 8.8f, height, 0));
        matrix.setTranslation(position);
        shader.bind();
        shader.setUniform("projection", camera.getProjection());
        shader.setUniform("model", matrix);
        shader.setUniform("color", color);
        model.render();
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public Vector3f getPosition() {
        return position;
    }

    public float getY() {
        return y;
    }
}
