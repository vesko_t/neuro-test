package graphics;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.*;

/**
 * Created by Veselin Trichkov on 20.03.18.
 */
public class MathModelNoIndices {
    private int vId, tId, count;

    float[] vertices;

    public MathModelNoIndices() {
        vertices = new float[]{
                -1f, 1f, //TOP LEFT
                1f, 1f,   //TOP LEFT
                1f, -1f, //BOTTOM RIGHT

                -1f, 1f, //TOP LEFT
                1f, -1f, //BOTTOM RIGHT
                -1f, -1f, //BOTTOM LEFT
        };

        float[] textures = new float[]{
                0, 0,
                1, 0,
                1, 1,

                0, 0,
                1, 0,
                0, 1
        };

        fillBuffers(vertices, textures);
    }

    public MathModelNoIndices(float[] textures) {
        vertices = new float[]{
                -1f, 1f, //TOP LEFT
                1f, 1f,   //TOP LEFT
                1f, -1f, //BOTTOM RIGHT
                -1f, -1f, //BOTTOM LEFT
        };
        fillBuffers(vertices, textures);
    }

    public MathModelNoIndices(float[] vertices, float[] textures) {
        fillBuffers(vertices, textures);
    }

    private void fillBuffers(float[] vertices, float[] texCoords) {
        this.vertices = vertices;
        count = vertices.length / 2;

        tId = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, tId);
        glBufferData(GL_ARRAY_BUFFER, MathModel.createBuffer(texCoords), GL_STATIC_DRAW);

        vId = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vId);
        glBufferData(GL_ARRAY_BUFFER, MathModel.createBuffer(vertices), GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }


    public void render() {

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        glBindBuffer(GL_ARRAY_BUFFER, vId);
        glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, tId);
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);

        glDrawArrays(GL_TRIANGLES, 0, count);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);


    }

    public float[] getVertices() {
        return vertices;
    }
}
