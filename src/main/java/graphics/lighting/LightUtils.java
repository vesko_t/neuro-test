package graphics.lighting;

import graphics.Texture;
import org.joml.Vector3f;
import rendering.Shader;

/**
 * Created by Veso on 9.3.2018 г..
 */
public class LightUtils {

    private static Shader shader;

    public LightUtils(Shader shader) {
        LightUtils.shader = shader;
    }

    public static Light createLight(String texture, Vector3f position, Vector3f scale) {
        return new Light(new Texture(texture), position, shader, scale);
    }

    public static Light createLight(Vector3f position, Vector3f scale) {
        return new Light(new Texture("/images/lightingV2.png"), position, shader, scale);
    }
}
