package graphics.lighting;

import graphics.Texture;
import org.joml.Vector3f;
import rendering.Renderable;
import rendering.Shader;

/**
 * Created by Veso on 11.3.2018 г..
 */
public abstract class Lightable extends Renderable {

    protected Light light;

    public Lightable(Texture texture, Shader shader, Vector3f scale, Vector3f position) {
        super(texture, shader, scale, position);
    }

    public void scaleLight(Vector3f scale) {
        light.setScale(scale);
    }

    public void renderLight() {
        light.render();
    }

    public Light getLight() {
        return light;
    }
}
