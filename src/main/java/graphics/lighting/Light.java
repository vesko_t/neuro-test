package graphics.lighting;

import graphics.Texture;
import org.joml.Vector3f;
import rendering.Renderable;
import rendering.Shader;

/**
 * Created by vesko on 09.03.18.
 */
public class Light extends Renderable {

    Light(Texture texture, Vector3f position, Shader shader, Vector3f scale) {
        super(texture,shader,scale,position);
    }

    public void setScale(Vector3f scale) {
        this.scale = scale;
        matrix.scale(scale);
    }
}
