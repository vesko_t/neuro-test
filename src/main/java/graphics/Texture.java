package graphics;

import de.matthiasmann.twl.utils.PNGDecoder;

import java.io.IOException;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

/**
 * Created by Veselin Trichkov on 22.1.2018 г..
 */
public class Texture {
    private int textureId;

    private int width;

    private int height;

    private ByteBuffer buffer;

    public Texture(String fileName) {

        ByteBuffer buffer = null;

        try {
            PNGDecoder decoder = new PNGDecoder(Class.class.getResourceAsStream(fileName));

            buffer = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());

            decoder.decode(buffer, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);

            buffer.flip();

            textureId = glGenTextures();

            glBindTexture(GL_TEXTURE_2D, textureId);

            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

            height = decoder.getHeight();
            width = decoder.getWidth();

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, decoder.getWidth(), decoder.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

        } catch (IOException e) {
            e.printStackTrace();
        }

        this.buffer = buffer;
    }

    public ByteBuffer getBuffer() {
        return buffer;
    }

    /**
     * binds texture to texture sampler
     *
     * @param sampler the id ot the sampler
     */
    public void bind(int sampler) {
        glActiveTexture(GL_TEXTURE0 + sampler);
        glBindTexture(GL_TEXTURE_2D, textureId);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}

