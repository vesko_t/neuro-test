package graphics.gui.listeners;

/**
 * Created by Veselin Trichkov on 30.03.2018.
 */
public interface GLListener {

    void actionPerformed();
}
