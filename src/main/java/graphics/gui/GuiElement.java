package graphics.gui;

import graphics.Texture;
import org.joml.Vector2f;
import org.joml.Vector3f;
import rendering.Renderable;
import rendering.Shader;

/**
 * Created by vesko on 26.02.18.
 */
public class GuiElement extends Renderable {


    public static boolean hasResized = false;

    protected Vector3f screenPosition;
    protected Vector2f screenScale;

    public GuiElement(Shader shader, String textureName, Vector3f position, Vector3f scale) {
        super(new Texture(textureName), shader, scale, position);

        this.shader = shader;
        screenScale = new Vector2f(scale.x, scale.y);
        this.scale = new Vector3f(1280 / scale.x, 720 / scale.y, 0);
        this.position = new Vector3f(1 / (640 / ((position.x + scale.x / 2) - 640)), 1 / (360 / (-(position.y + scale.y / 2) + 360)), 0);
        screenPosition = position;
    }

    @Override
    public Vector3f getPosition() {
        return screenPosition;
    }

    @Override
    public void setPosition(Vector3f position) {
        this.position = new Vector3f(1 / (640 / ((position.x + scale.x / 2) - 640)),
                1 / (360 / (-(position.y + scale.y / 2) + 360)), 0);
    }

    public void render() {
        shader.bind();
        shader.setUniform("sampler", 0);
        shader.setUniform("position", position);
        shader.setUniform("scale", scale);
        texture.bind(0);
        model.render();
    }
}
