package graphics.gui;

import graphics.gui.listeners.GLListener;
import io.Input;
import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;
import rendering.Shader;

/**
 * Created by Veselin Trichkov on 29.03.18.
 */
public class GLButton extends GuiElement {

    private Input input;
    private GLListener listener;

    public GLButton(Shader shader, String textureName, Vector3f position, Vector3f scale, Input input) {
        super(shader, textureName, position, scale);
        this.input = input;
    }

    private boolean btwn(float value, float v1, float v2) {
        return value > v1 && value < v2;
    }

    private boolean isMouseOver() {
        return btwn(input.getMousePositionScreen().x, screenPosition.x, screenPosition.x + screenScale.x) &&
                btwn(input.getMousePositionScreen().y, screenPosition.y, screenPosition.y + screenScale.y);
    }

    public void setListener(GLListener listener) {
        this.listener = listener;
    }

    private void action() {
        if (input.isMouseButtonDown(GLFW.GLFW_MOUSE_BUTTON_1))
            if (isMouseOver())
                if (listener != null)
                    listener.actionPerformed();

    }

    @Override
    public void render() {
        action();
        super.render();
    }
}
