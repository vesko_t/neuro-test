package graphics.gui;

import io.Input;
import rendering.Shader;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Veselin Trichkov on 29.03.18.
 */
public class Menu {

    List<GLButton> buttons = new ArrayList<GLButton>();
    private Vector2f position;
    private Vector2f size;
    private Shader shader;
    private Input input;

    public Menu(Vector2f position, Vector2f size, Shader shader, Input input) {
        this.position = position;
        this.size = size;
        this.shader = shader;
        this.input = input;
    }

    public GLButton newButton(String texture, Vector3f size, Vector2f position) {
        GLButton button = new GLButton(shader, texture, new Vector3f(this.position.x + position.x, this.position.y + position.y, 0), size, input);
        return button;
    }

    public void addButton(GLButton button) {
        buttons.add(button);
    }

    public void render() {
        for (GLButton button : buttons) {
            button.render();
        }
    }
}
