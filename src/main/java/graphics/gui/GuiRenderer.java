package graphics.gui;

import graphics.bars.Bar;
import io.Input;
import rendering.Camera;
import rendering.Shader;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vesko on 20.02.18.
 */
public class GuiRenderer {

    private List<Bar> barList = new ArrayList<Bar>();
    private List<GuiElement> guiElements = new ArrayList<GuiElement>();
    private List<Menu> menuList = new ArrayList<Menu>();
    private Camera camera;
    private Shader shader;
    private Input input;

    public GuiRenderer(Camera camera, Shader shader, Input input) {
        this.camera = camera;
        this.shader = shader;
        this.input = input;
    }

    public void addBar(Bar bar) {
        barList.add(bar);
    }

    public Menu newMenu(Vector2f position, Vector2f size) {
        return new Menu(position, size, shader, input);
    }

    public void addMenu(Menu menu) {
        menuList.add(menu);
    }

    public void addElement(GuiElement element) {
        guiElements.add(element);
        //System.out.println(element.getPosition().x + "\t" + element.getPosition().y);
    }

    /**
     * renders all gui elements and bars
     */
    public void render() {
        for (Bar bar : barList) {
            bar.getPosition().x = -camera.getPosition().x - camera.getWidth() / 2 - bar.getValue() / 2;
            bar.getPosition().y = -camera.getPosition().y + camera.getHeight() / 2 - bar.getHeight() - bar.getY();
            bar.render(camera);
        }
        for (GuiElement element : guiElements) {
            element.render();
        }
        for (Menu menu : menuList) {
            menu.render();
        }
    }

}
