import entities.Entity;
import entities.ObjectRenderer;
import graphics.Texture;
import io.Window;
import org.joml.Vector3f;
import rendering.Camera;
import rendering.Shader;
import scene.Scenery;
import scene.SceneryRenderer;

public class Main {

    private Main() {

        Window window = new Window(1280, 720, "Pesho", false);

        Camera camera = new Camera(window);

        SceneryRenderer sceneryRenderer = new SceneryRenderer();

        ObjectRenderer objectRenderer = new ObjectRenderer();

        double frameCap = 1.0 / 60.0;

        double frameTime = 0;
        int frames = 0;

        double time = (double) System.nanoTime() / (double) 1000000000;

        double unprocessed = 0;

        int i = 0;

        objectRenderer.getEntities().add(new Entity(new Vector3f(50, 500, 0), new Vector3f(32), new Texture("/images/enemy.png"), false));

        sceneryRenderer.getSceneries().add(new Scenery(new Texture("/images/grass.png"), new Shader("scenery"), new Vector3f(10000, 32, 0), new Vector3f(0, 32, 0)));
        sceneryRenderer.getSceneries().add(new Scenery(new Texture("/images/tile2.png"), new Shader("scenery"), new Vector3f(32), new Vector3f(1000, 128, 0)));

        while (!window.shouldClose()) {

            boolean canRender = false;

            double time2 = (double) System.nanoTime() / (double) 1000000000;
            double past = time2 - time;
            unprocessed += past;
            frameTime += past;

            time = time2;

            while (unprocessed >= frameCap) {
                unprocessed -= frameCap;
                canRender = true;

                window.prepare();

                objectRenderer.update((float) frameCap);

                camera.update(objectRenderer.getEntities().get(0));
                //System.out.println(scenery.getPosition().x + "\t" + scenery.getPosition().y);
                System.out.println(objectRenderer.getEntities().get(0).getPosition().x + "\t" + objectRenderer.getEntities().get(0).getPosition().y);

                window.getInput().update();

                if (frameTime >= 1.0) {
                    frameTime = 0;
                    i++;
                    if (i > 5)
                        i = 0;
                    System.out.println("FPS:" + frames);
                    frames = 0;
                }
            }
            if (canRender) {
                window.clear();

                sceneryRenderer.render();

                objectRenderer.render();

                window.swapBuffers();
                frames++;
            }
        }
        window.terminate();
    }

    public static void main(String[] args) {
        new Main();
    }

}
