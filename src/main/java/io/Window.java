package io;

import graphics.Texture;
import org.lwjgl.glfw.GLFWImage;
import org.lwjgl.glfw.GLFWVidMode;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL.createCapabilities;
import static org.lwjgl.opengl.GL11.*;

/**
 * Created by Veso on 22.1.2018 г..
 */
public class Window {

    long window;
    private static int height, width;
    private boolean fullScreen;
    private Input input;

    public Window(int width, int height, String title, boolean fullScreen) {

        this.fullScreen = fullScreen;
        Window.height = height;
        Window.width = width;


        if (!glfwInit()) {
            System.err.println("Failed to initialize GLFW");
            System.exit(1);
        }

        window = glfwCreateWindow(width, height, title, fullScreen ? glfwGetPrimaryMonitor() : 0, 0);
        glfwSetWindowSizeCallback(window, new WindowCallback(window));


        if (!fullScreen) {
            GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            glfwSetWindowPos(window, (vidMode.width() - 1280) / 2, (vidMode.height() - 720) / 2);
        }

        glfwMakeContextCurrent(window);

        createCapabilities();

        input = new Input(this);
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        /*Texture texture = new Texture("/images/buggerCursor.png");

        GLFWImage image = GLFWImage.malloc().set(texture.getWidth(), texture.getHeight(), texture.getBuffer());

        long cursor = glfwCreateCursor(image, 0, 0);

        glfwSetCursor(window, cursor);*/
    }

    public void prepare() {
        glfwPollEvents();
    }

    public void clear() {
        glClear(GL_COLOR_BUFFER_BIT);
    }

    public void swapBuffers() {
        glfwSwapBuffers(window);
    }

    public void close() {
        glfwSetWindowShouldClose(window, true);
    }

    public boolean shouldClose() {
        return glfwWindowShouldClose(window);
    }

    public static int getHeight() {
        return height;
    }

    public static void setHeight(int height) {
        Window.height = height;
    }

    public static void setWidth(int width) {
        Window.width = width;
    }

    public static int getWidth() {
        return width;
    }

    public Input getInput() {
        return input;
    }

    public void terminate() {
        glfwTerminate();
    }
}

