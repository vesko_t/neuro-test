package io;

/**
 * Created by Veso on 28.12.2017 г..
 */
public class Timer {
    public static double getTime() {
        return (double) System.nanoTime() / (double) 1000000000;
    }
}
