package io;

import graphics.gui.GuiElement;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.system.NativeType;

import static org.lwjgl.glfw.GLFW.glfwGetWindowSize;
import static org.lwjgl.opengl.GL11.glViewport;

/**
 * Created by Veselin Trichkov on 29.03.18.
 */
public class WindowCallback extends GLFWWindowSizeCallback {

    long window;

    public WindowCallback(long window) {
        this.window = window;
    }

    public void invoke(@NativeType("GLFWwindow *") long l, int width, int height) {

        //System.out.println("Test");
    }

    public String getSignature() {
        return null;
    }

    public void callback(long args) {
        int width[] = new int[1];
        int height[] = new int[1];
        glfwGetWindowSize(window, width, height);
        Window.setWidth(width[0]);
        Window.setHeight(height[0]);
        glViewport(0, 0, Window.getWidth(), Window.getHeight());
        GuiElement.hasResized = true;
    }

    public void close() {

    }
}
