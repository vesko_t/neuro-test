package io;

/**
 * Created by Veso on 22.1.2018 г..
 */

import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.*;

public class Input {

    private static Window window;

    private static boolean[] keys;

    private static boolean[] buttons;

    public Input(Window window) {
        this.window = window;
        keys = new boolean[GLFW_KEY_LAST];
        for (int i = GLFW_KEY_SPACE; i < GLFW_KEY_LAST; i++) {
            keys[i] = false;
        }
        buttons = new boolean[3];
        for (int i = 0; i < 3; i++) {
            buttons[i] = false;
        }
    }

    public static boolean isKeyDown(int key) {
        return glfwGetKey(window.window, key) == 1;
    }


    public static boolean isMouseButtonDown(int button) {
        return glfwGetMouseButton(window.window, button) == 1;
    }

    public void update() {
        for (int i = GLFW_KEY_SPACE; i < GLFW_KEY_LAST; i++) {
            keys[i] = isKeyDown(i);
        }
        for (int i = 0; i < 3; i++) {
            buttons[i] = isMouseButtonDown(i);
        }
    }

    public static Vector3f getMousePositionScreen() {
        double xPos[] = new double[100];
        double yPos[] = new double[100];
        glfwGetCursorPos(window.window, xPos, yPos);

        return new Vector3f((float) xPos[0], (float) yPos[0], 0);
    }

    public static Vector3f getMousePositionCamera() {
        double xPos[] = new double[100];
        double yPos[] = new double[100];
        glfwGetCursorPos(window.window, xPos, yPos);

        return new Vector3f((float) (xPos[0] - Window.getWidth() / 2), (float) ((yPos[0] - Window.getHeight() / 2) * -1), 0);
    }

    public static boolean isMouseButtonPressed(int button) {
        return (isMouseButtonDown(button) && !buttons[button]);
    }

    public static boolean isKeyPressed(int key) {
        return (isKeyDown(key) && !keys[key]);
    }

}
